import connectDB from "@/config/database";

//GET /api/properties
export const GET = async (request) => {
  try {
    await connectDB();

    const properties = await Property.find({ isFeatured: true });

    return Response.json(properties);
  } catch (error) {
    console.log(error);
    return new Response("Someting Went Worng", { status: 500 });
  }
};
