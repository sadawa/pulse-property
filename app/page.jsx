import React from "react";
import Hero from "@/components/Hero";
import InfoBoxes from "@/components/InfoBoxes";
import HomePropreties from "@/components/HomePropreties";
import FeaturedProperties from "@/components/FeaturedProperties";

export const metadata = {
  title: "HomePage Pulse",
};

const HomePage = () => {
  return (
    <>
      <Hero />
      <InfoBoxes />
      <FeaturedProperties />
      <HomePropreties />
    </>
  );
};

export default HomePage;
