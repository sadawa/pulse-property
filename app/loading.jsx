"use client";
import React from "react";
import CircleLoader from "react-spinners/CircleLoader";

const override = {
  display: "block",
  margin: "100px auto",
};

const LoadingPage = ({ loading }) => {
  return (
    <CircleLoader
      color="#3b82f6"
      loading={loading}
      cssOverride={override}
      size={250}
      aria-label="Loading Spinner"
    />
  );
};

export default LoadingPage;
